$(document).ready(function() {
  const $videoButton = $('.video__button');
  const $videoLightbox = $('.video-lightbox');
  const $videoLightboxOverlay = $('.video-lightbox__overlay');
  const $body = $('body');

  $videoButton.click(function() {
    $videoLightbox.addClass('video-lightbox--is-visible');
    $body.addClass('video-lightbox-is-visible')
  });

  $videoLightboxOverlay.click(function() {
    $videoLightbox.removeClass('video-lightbox--is-visible');
    $body.removeClass('video-lightbox-is-visible')
  });
});