import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public money_lost:number = 0 ;
  constructor(){
    this.init();
  }
  private init(){
    let money_lost_origin_value = 1000;
    let money_lost_origin_date = new Date(2018, 0, 1);
    let money_lost_per_second = .05;
    let interval = this.upcountWorker("money_lost", this, ,money_lost_origin_value)
  }
  public upcountWorker(property:string, scope:any, initialValue:number, intialDate:Date=new Date(2018, 0, 1), incrementValue:number=.05, incrementInterval:number=1000):number{
    let current_date = new Date();
    let time_difference_from_origin = current_date.getTime() - intialDate.getTime();
    scope[property] = Math.round((((time_difference_from_origin/1000) * incrementValue) + initialValue) * 100)/100;
    return <number> (<any>setInterval(()=>{
      scope[property] = Math.round((this.money_lost + incrementValue) * 100)/100;
    }, incrementInterval));
  }

}
