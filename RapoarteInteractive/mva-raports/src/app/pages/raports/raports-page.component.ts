import { IncrementalWorker } from './../../services/increment-worker.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-raports-page',
  templateUrl: './raports-page.component.html',
  styleUrls: ['./raports-page.component.scss']
})
export class RaportsPageComponent implements OnInit {
  worker1: IncrementalWorker;
  worker2: IncrementalWorker;
  worker3: IncrementalWorker;
  worker4: IncrementalWorker;
  constructor() {
    this.worker1 = new IncrementalWorker(1000, 0.05, new Date(2018, 0, 1), 1000);
    this.worker2 = new IncrementalWorker(100, 0.08, new Date(2016, 0, 1), 2000);
    this.worker3 = new IncrementalWorker(500, 0.09, new Date(2015, 0, 1), 100);
    this.worker4 = new IncrementalWorker(35, 0.01, new Date(2012, 0, 1), 500);
  }

  ngOnInit() {
  }

}
