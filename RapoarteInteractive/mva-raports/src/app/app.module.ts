import { RaportsPageComponent } from './pages/raports/raports-page.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header/header.component';
import { FooterComponent } from './shared/footer/footer/footer.component';
import { RaportComponent } from './shared/raport/raport.component';

@NgModule({
  declarations: [
    AppComponent,
    RaportsPageComponent,
    HeaderComponent,
    FooterComponent,
    RaportComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
