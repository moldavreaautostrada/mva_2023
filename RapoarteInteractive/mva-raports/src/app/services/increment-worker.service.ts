export class IncrementalWorker {
  public money_lost = 0 ;
  constructor(originValue: number, valueLostPerSecond: number, originDate: Date, interval: number) {
    this.init(originValue, valueLostPerSecond, originDate, interval);
  }

  private init(originValue: number, valueLostPerSecond: number, originDate: Date, intervalValue: number) {
    const money_lost_origin_value = originValue;
    const money_lost_origin_date = originDate;
    const money_lost_per_second = valueLostPerSecond;
    const interval = this.upcountWorker('money_lost', this, money_lost_origin_value, money_lost_origin_date,
          money_lost_per_second, intervalValue);
  }

  public upcountWorker(property: string, scope: any, initialValue: number, intialDate: Date = new Date(2018, 0, 1),
      incrementValue: number = .05, incrementInterval: number = 1000): number {
    const current_date = new Date();
    const time_difference_from_origin = current_date.getTime() - intialDate.getTime();
    scope[property] = Math.round((((time_difference_from_origin / 1000) * incrementValue) + initialValue) * 100) / 100;
    return <number> (<any>setInterval(() => {
      scope[property] = Math.round((this.money_lost + incrementValue) * 100) / 100;
    }, incrementInterval));
  }
}
