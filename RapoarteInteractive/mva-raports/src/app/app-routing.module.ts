import { RaportsPageComponent } from './pages/raports/raports-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'rapoarte', component: RaportsPageComponent, pathMatch: 'full' },
  { path: '', redirectTo: 'rapoarte', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
