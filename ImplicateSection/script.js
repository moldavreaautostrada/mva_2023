$(document).ready(function(){	
	function toDegrees(angle){
		return angle * (Math.PI / 180);
	}
	console.clear();
	var parent = $('div.parent');
	var parentLeft = parent.offset().left;
  var parentTop = parent.offset().top;
	var parentLeftCentre = parentLeft + (parent.width() / 2);
	var parentTopCentre = parentTop + (parent.width() / 2);
	
	//Number of services
	var noOfServices = 6;
	//Half width of parent
	var parentCircleHalfWidth = 75; 
	//Add some padding from parent circle
	var Hyp = parentCircleHalfWidth + 100; 
	//360 degrees / the number of services
	var angle = Math.round(360 / noOfServices); 
	
	for(var i = 1; i <= noOfServices; i++){ 
		var currentAngle = i * angle;
		//calculate the positioning of the child circle around the parent circle
		var circlePosXOpp = Math.sin(toDegrees(currentAngle)) * Hyp;
		var circlePosYAdj = Math.cos(toDegrees(currentAngle)) * Hyp;
		//create a child element and assign "child" class with some properties such as positining
var currentChild = $('<div class="child"></div>').appendTo('div.parent');		
		currentChild.css({'transform' : 'translate(' + circlePosXOpp + 'px, ' + -circlePosYAdj + 'px)'});
	}
});